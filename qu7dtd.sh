#!/bin/bash
set -euo pipefail

# keep this simplistic - used as folder name, service name etc
GAMESHORT="7days2die"
GAMEDIR="$HOME/${GAMESHORT}"
GAMEID=294420
GAMEBINDIR=$GAMEDIR
GAMEBIN=$GAMEDIR/7DaysToDieServer.x86_64

_SELF=$(readlink -f "$0")
QUDIR=$(dirname "$_SELF")
QUCFG="${QUDIR}/serverconfig.xml"
QUCFG_dest="$GAMEDIR/serverconfig.qu.xml"
QUPRIVATE="${QUDIR}/private" # expected to be sourcable

GAMEOPTS="-configfile=${QUCFG_dest} -logfile output.log -quit -batchmode -nographics -dedicated"


action_install() {
	# $ useradd steam; su steam -c this-script

	if [[ ! -d "$HOME/Steam" ]]; then
		mkdir $HOME/Steam

		cd $HOME/Steam/
		curl -sqL 'https://steamcdn-a.akamaihd.net/client/installer/steamcmd_linux.tar.gz' | tar zxvf -
	fi

	mkdir -p $GAMEDIR
	if [[ ! -d "$GAMEBIN" ]]; then
		$HOME/Steam/steamcmd.sh +force_install_dir $GAMEDIR +login anonymous +app_update $GAMEID +quit
	fi

    action_service_update

    action_config_update
}

make_savebackup() {
    if [[ -d "$GAMEDIR/Saves" ]]; then
        cd "$GAMEDIR"
        if [[ ! -d backups ]]; then
            echo "Backups not possible - create/mount/link ${GAMEDIR}/backups directory!" >&2
            exit 1
        fi
        rdiff-backup ./Saves ./backups/Saves
        rdiff-backup --remove-older-than 2D --force ./backups/Saves
    fi
}

action_backup() {
    server_console_command "saveworld"
    make_savebackup
}

action_update() {
    make_savebackup
    $HOME/Steam/steamcmd.sh +force_install_dir $GAMEDIR +login anonymous +app_update $GAMEID validate +quit
}

action_config_check() {
    [[ -f "$QUPRIVATE" && -x "$QUPRIVATE" ]] || echo "Create $QUDIR/private (see private.example), and make it executable (chmod +x) before starting server"
    :
}
action_config_update() {
    if [[ "${CLEAN_CONFIGS:-false}" == "true" ]]; then
        rm -f "$QUCFG_dest"
        if [[ -h "${GAMEDIR}/Mods" ]]; then
            rm -f "${GAMEDIR}/Mods"
        elif [[ -d "${GAMEDIR}/Mods" ]]; then
            mv "${GAMEDIR}/Mods" "${GAMEDIR}/Mods_original" || { echo "Unable to rename Mods dir" >&2; exit 1; }
        fi
        if [[ -d "${GAMEDIR}/Saves" ]]; then
            if [[ ! -d "${GAMEDIR}/Saves_backup" ]]; then
                mv "${GAMEDIR}/Saves" "${GAMEDIR}/Saves_backup"
            else
                echo "WARNING: Wiping Saves directory (since Saves_backup exists)"
                echo -n "abort now if not desired..."
                echo -n "3..."
                sleep 0.5 || return 1
                echo -n "2 ..."
                sleep 0.5 || return 1
                echo -n "1 ..."
                sleep 0.5 || return 1
                echo ""
                # backup already there, assume current Saves is just clean/from fresh start and wipe it
                rm -rf "${GAMEDIR}/Saves"
            fi
            mkdir "${GAMEDIR}/Saves"
        fi
        return
    fi
    cp "$QUCFG" "$QUCFG_dest"
    if [[ -d "${GAMEDIR}/Mods" && ! -h "${GAMEDIR}/Mods" ]]; then
        mv "${GAMEDIR}/Mods" "${GAMEDIR}/Mods_original";
    fi
    if [[ -d "${QUDIR}/Mods" && ! -e "${GAMEDIR}/Mods" ]]; then
        ln -snf "${QUDIR}/Mods/" "${GAMEDIR}/Mods"
    fi

    if [[ -x "$QUPRIVATE" ]]; then
        source "$QUPRIVATE" "$QUCFG_dest"
    fi

    action_config_check
}

action_service_update() {
    tee ${GAMEDIR}/7dtd.service <<EOF
[Unit]
Description=${GAMESHORT}
Wants=network-online.target
After=syslog.target network.target nss-lookup.target network-online.target

[Service]
ExecStart=${_SELF} start
LimitNOFILE=100000
LimitCORE=0
ExecStop=${_SELF} stop
User=steam
Group=steam

[Install]
WantedBy=multi-user.target
EOF
}

action_start() {
    action_config_update

    OPTS="${OPTS:-}"
    OPTS="${OPTS} ${GAMEOPTS}"

    if [[ "${CLEAN_CONFIGS:-false}" == "true" ]]; then
        OPTS="-configfile=${GAMEDIR}/serverconfig.xml -quit -batchmode -nographics -dedicated"
    fi

    cd $GAMEBINDIR
    ulimit -c 0
    export LD_LIBRARY_PATH=.
    exec $GAMEBIN $OPTS
}

server_console_command() {
    local cmd="$1"
    local tpass=$(sed -nr 's/.*TelnetPass.*value="(.*)".*/\1/p' "$QUCFG_dest")
    local tport=$(sed -nr 's/.*TelnetPort.*value="(.*)".*/\1/p' "$QUCFG_dest")
    set +e
    local OUT=$( (sleep 0.2; echo "$tpass"; sleep 0.2; echo "$cmd"; sleep 1 ) | telnet 127.0.0.1 $tport | tr -d '\0' 2>&1 )
    echo "$OUT"
    grep -q "Connection closed" <<<"$OUT"
}

action_stop() {
    server_console_command "shutdown"
}

case "${1:-start}" in
    install)
        action_install
        exit
        ;;
    update)
        action_stop
        action_update
        exit
        ;;
    start)
        action_start
        ;;
    clean-start)
        export CLEAN_CONFIGS=true
        action_start
        ;;
    stop)
        action_stop
        ;;
    restart)
        action_stop
        action_start
        ;;
    backup)
        action_backup
        ;;
    *)
        echo "Unrecognized action $1, supported are: install, update, start, stop, restart" >&2
        exit 1
esac

